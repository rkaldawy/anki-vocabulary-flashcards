""" Creates the note model for automatically generated Japanese Anki cards. 
"""

import os
from aqt.utils import askUser, showInfo
from anki.lang import _

_field_names = ["Word", "Definitions"]
_model_name = "English Vocabulary (Auto-Generated)"

dir_path = os.path.dirname(os.path.realpath(__file__))
with open(os.path.join(dir_path, 'html/qfmt.html'), 'r') as f:
    qfmt = f.read()
with open(os.path.join(dir_path, 'html/afmt.html'), 'r') as f:
    afmt = f.read()

""" Creates the model used for the auto-generated notes. 
"""
def create_model(mw):
    mm = mw.col.models
    m = mm.new(_(_model_name))

    for field_name in _field_names:
        fm = mm.newField(_(field_name))
        mm.addField(m, fm)  

    t = mm.newTemplate("Card 1")
    t['qfmt'] = qfmt
    t['afmt'] = afmt
    mm.addTemplate(m, t)

    mm.add(m)
    mw.col.models.save(m)
    return m

""" Fetches the model for the auto-generated notes, and creates it if it does not yet exist. 
    Updates model if new fields have been added.
"""
def get_vocabulary_model(mw):
    m = mw.col.models.by_name(_model_name)
    if not m:
        showInfo("Vocabulary note type not found. Creating...")
        m = create_model(mw)

    # Add new fields if they don't exist yet
    fields_to_add = [field_name for field_name in _field_names if field_name not in mw.col.models.field_names(m)]
    if fields_to_add:
        showInfo("""
        <p>The vocabulary plugin has recently been upgraded to include the following attributes: {}</p>
        <p>This change will require a full-sync of your card database to your Anki-Web account.</p>
        """.format(", ".join(fields_to_add)))
        for field_name in fields_to_add:
            fm = mw.col.models.newField(_(field_name))
            mw.col.models.addField(m, fm)
            mw.col.models.save(m)

    return m