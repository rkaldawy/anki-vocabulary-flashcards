""" A plugin for Anki which automatically generates Japanese vocabulary flashcards. Cards are
    populated using Jisho.  
    Based heavily off of https://github.com/JASchilz/AnkiSyncDuolingo/blob/master/duolingo_sync/
"""

from aqt import mw
from aqt.qt import *
from aqt.utils import showInfo, askUser, showWarning
from anki.utils import split_fields, ids2str

from .vocabulary_model import get_vocabulary_model
from .vocabulary_note import create_note
from .vocabulary_dialog import vocabulary_dialog

""" Main function for the plugin. Collects a list of words to add, auto-generates notes, and adds 
    them to special deck.
"""
def add_cards():
    model = get_vocabulary_model(mw)

    if not model:
        showWarning("Could not get the vocabulary note type.")
        return
    
    note_ids = mw.col.find_notes('tag:vocabulary_automated')
    notes = mw.col.db.list("select flds from notes where id in {}".format(ids2str(note_ids)))
    existing_words = [split_fields(note)[0] for note in notes]

    did = mw.col.decks.id("English Vocabulary (Auto-generated)")
    mw.col.decks.select(did)

    deck = mw.col.decks.get(did)
    deck['mid'] = model['id']
    mw.col.decks.save(deck)

    words_to_add = vocabulary_dialog(mw)
    success_count = 0
    repeated_count = 0
    fail_words = []

    for word in words_to_add:
        n = create_note(mw, word)
        if not n:
            fail_words.append(word)
            continue
        elif n['Word'] in existing_words:
            repeated_count += 1
            continue
        mw.col.add_note(n, did)
        success_count += 1

    message = "{} words successfully added.".format(success_count)
    if repeated_count > 0:
        message += " {} words already existed in the deck.".format(repeated_count)
    if fail_words:
        message += " Failed to add words: {}.".format(", ".join(fail_words))

    showInfo(message)
    mw.moveToState("deckBrowser")
    
action = QAction("Auto-generate Enlgish vocabulary notes", mw)
action.triggered.connect(add_cards)
mw.form.menuTools.addAction(action)