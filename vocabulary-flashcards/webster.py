""" Methods for querying Jisho for vocabulary information. 
"""

import requests
import json
import re
import os
from collections import defaultdict

REQUEST_QUERY = "https://www.dictionaryapi.com/api/v3/references/collegiate/json/{}?key={}"

REGEX_DICT = {
    r'^{bc}': r'', # colon
    r'(?<!^) {bc}': r'; ', # semicolon
    r'{\'t\': (.*)}': r'\1', # example formatting
    r'{wi}(.*?){/wi}': r'<em>\1</em>', # italic marker
    r'{it}(.*?){/it}': r'<em>\1</em>', # italic marker
    r'{a_link\|(.*?)}': r'(\1)', # hyperlink
    r'{d_link\|([^\|]*)\|.*?}': r'\1', # hyperlink
    r'{sx\|([^\|]*?)\|\|}': r'\1', # caps
    r'{dx_def}(.*?){/dx_def}': r'' # definition ref 
}

def run_regex(s):
    for k, v in REGEX_DICT.items():
        s = re.sub(k, v, s)
    return s

def json_print(o):
    print(json.dumps(o, indent=2))

class ExampleEntry:

    EXAMPLE = """ 
    <div class = "example">
        {}
    </div>
    """

    def __init__(self, data):
        self.text = data['t']

        self.author = ""
        if "aq" in data.keys():
            if "auth" in data['aq'].keys():
                self.author = data['aq']['auth']
            elif "source" in data['aq'].keys():
                self.author = data['aq']['source']
    
    def to_html(self):
        ret = ""

        ret += self.EXAMPLE.format(run_regex(self.text))
        if self.author != "":
            author_str = "- " + run_regex(self.author)
            ret += self.EXAMPLE.format(author_str)

        return ret 

class DefinitionEntry:

    DEFINITION =  """
    <div class = "definition">
        {}
    </div>
    """

    def __init__(self, data):
        self.text = ""
        self.examples = []
        self.note = []
        self.subentries = []

        for item in data['dt']:
            if item[0] == 'text':
                self.text = item[1] 
            elif item[0] == 'vis':
                for example in item[1]:
                    self.examples.append(ExampleEntry(example))
            elif item[0] == 'uns':
                self.note.append(item[1])
            
        if 'sdsense' in data.keys():
            self.subentries.append(DefinitionSubentry(data['sdsense']))
    
    def to_html(self):
        ret = ""

        ret += self.DEFINITION.format(run_regex(self.text))
        for example in self.examples:
            ret += example.to_html()
        for subentry in self.subentries:
            ret += subentry.to_html()

        return ret


class DefinitionSubentry(DefinitionEntry):

    ITALICS = """<em>{}</em>"""

    def __init__(self, data):
        super().__init__(data)
        self.type = data['sd'] 
    
    def italicize(self, s):
        return self.ITALICS.format(s)
    
    def to_html(self):
        ret = ""

        text =  self.italicize(run_regex(self.type)) + ": " + run_regex(self.text)
        ret += self.DEFINITION.format(text)
        for example in self.examples:
            ret += example.to_html()
        for subentry in self.subentries:
            ret += subentry.to_html()

        return ret

class DefinitionSense:

    TABLE_ROW = """
    <tr>
        {}
    </tr>
    """

    TABLE_INDEX = """
    <th>
        {}
    </th>
    """

    TABLE_ENTRY = """
    <td>
    <div class = "entry">
        {}
    </div>
    </td>
    """

    SUBITEM = """
    <div class = "subitem">
        {}
    </div>
    """

    def __init__(self, data):
        self.entries = []
        for item in data:
            if item[0] != "sense":
                continue 
            self.entries.append(DefinitionEntry(item[1]))
    

    def to_html(self, temp):

        def add_index(r, s):
            return r + self.TABLE_INDEX.format(s)

        def add_entry(r, s):
            return r + self.TABLE_ENTRY.format(s)

        def add_row(t, r):
            return t + self.TABLE_ROW.format(r)

        ret = ""

        index = 0
        header_added = False
        for entry in self.entries:
            row = ""

            if not header_added:
                row = add_index(row, temp)                
                header_added = True
            else:
                row = add_index(row, "") 

            index_str = chr(index + 97) + "."
            row = add_index(row, self.SUBITEM.format(index_str)) 
            row = add_entry(row, entry.to_html())

            index += 1
            ret = add_row(ret, row)
        
        return ret


class Definition:
    TABLE = """
    <table style="width: 100%">
        {}
    </table>
    """

    TABLE_SPACER = """
    <tr style="height:20px"> </tr>
    """

    def __init__(self, word, data):
        self.word = word.split(":")[0] #temporary workaround
        self.senses = []
        for item in data:
            self.senses.append(DefinitionSense(item))
    
    def to_html(self):
        ret = ""

        index = 0
        for sense in self.senses:
            index_str = str(index + 1) + "."
            r = sense.to_html(index_str)
            ret += r
            ret += self.TABLE_SPACER 
            index += 1

        ret = self.TABLE.format(ret) 
        return ret

def query_word(word):
    dirpath = os.path.dirname(os.path.realpath(__file__))
    key = open(dirpath + "/API_KEY.txt", "r").read()
    query = REQUEST_QUERY.format(word, key)
    r = requests.get(query)
    return json.loads(r.text)

def lookup_word(word):
    data = query_word(word)[0]
    ret = Definition(data['meta']['id'], data['def'][0]['sseq'])
    return ret