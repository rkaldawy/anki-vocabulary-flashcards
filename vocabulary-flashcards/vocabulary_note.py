""" Methods for creating auto-generated Japanese notes.
"""
from .webster import lookup_word 
from collections import defaultdict

""" Creates a note. Some fields have HTML/CSS styling baked in to represent lists, line breaks, 
    etc.
"""
def create_note(mw, word):

    r = None
    try:
        r = lookup_word(word)
    except:
        return None
    if not r:
        return None

    n = mw.col.newNote()
    n._fmap = defaultdict(str, n._fmap)

    n['Word'] = r.word
    n['Definitions'] = r.to_html()

    n.add_tag('vocabulary_automated')

    return n