""" Methods for the addon user interface.
"""

from aqt.qt import *
from anki.lang import _

""" Queries the user for desired Japanese words. Words must be newline-delimited. 
"""
def vocabulary_dialog(mw):
    d = QDialog(mw)
    d.setWindowTitle("Autogenerate English Vocabulary notes")
    d.setWindowModality(Qt.WindowModality.WindowModal)
    vbox = QVBoxLayout()
    l = QLabel("""
    <p>This plugin will autogenerate Anki notes and cards for English vocabulary. The addon takes newline-delimited 
    Japanese words in the textbox below, creates new notes, and places them in a special deck.</p>
    """)
    l.setOpenExternalLinks(True)
    l.setWordWrap(True)
    vbox.addWidget(l)
    vbox.addSpacing(20)
    g = QGridLayout()
    l1 = QLabel(_("Words to add:"))
    g.addWidget(l1, 0, 0)
    words = QTextEdit()
    words.setFontPointSize(12)
    g.addWidget(words, 0, 1)
    vbox.addLayout(g)
    bb = QDialogButtonBox(QDialogButtonBox.StandardButton.Ok | QDialogButtonBox.StandardButton.Cancel)
    bb.button(QDialogButtonBox.StandardButton.Ok).setAutoDefault(True)
    bb.accepted.connect(d.accept)
    bb.rejected.connect(d.reject)
    vbox.addWidget(bb)
    d.setLayout(vbox)
    d.show()
    accepted = d.exec()

    ret = words.toPlainText().split("\n")
    ret = [elt for elt in ret if elt]
    return ret