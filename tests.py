import json
import jsonpickle

import sys
sys.path.append('vocabulary-flashcards/')

from webster import query_word, lookup_word

def json_print(o):
    print(json.dumps(o, indent=2))

#json_print(query_word("sputtering"))
print(lookup_word("culottes").to_html())