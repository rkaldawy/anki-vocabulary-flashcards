ADDON_DIR=$(HOME)/.local/share/Anki2/addons21

test:
	ln -s $(shell pwd)/vocabulary-flashcards $(ADDON_DIR)

install:
	cp -r vocabulary-flashcards $(ADDON_DIR)/vocabulary-flashcards

package:
	rm -rf ./vocabulary-flashcards/__pycache__
	zip -r package.zip ./vocabulary-flashcards/
	mv package.zip package.ankiaddon

clean:
	rm -rf $(ADDON_DIR)/vocabulary-flashcards
